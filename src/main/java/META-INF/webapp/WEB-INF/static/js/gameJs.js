//$(document).ready(function(){
//    $('#myCanvas').css('display', 'block');
//    $('#showGrid').css('display','none');
//
//    var startUrl = document.URL;
//
//     $.ajax({
//            type: 'GET',
//            url: startUrl,
//            beforeSend: function (request){
//            request.setRequestHeader('accept', 'application/json');
//            }
//            }).done(function ( data ) {
//            console.log(data);
//            var canvas=document.getElementById('myCanvas');
//            var ctx=canvas.getContext('2d');
//            ctx.fillStyle='000000';
//            for (var dot in data) {
//            console.log(data[dot].x, data[dot].y);
//            var x = parseInt(data[dot].x) * 5;
//            var y = parseInt(data[dot].y) * 5;
//            ctx.fillRect(x, y, 5, 5);
//            }
//            });
//
//     function loadGeneration(urlToCall) {
//
//        var url = urlToCall.split('/');
//        $.ajax({
//            type: 'GET',
//            url: '/game/test',
//            beforeSend: function (request){
//                request.setRequestHeader('accept', 'application/json');
//            }
//        }).done(function ( data ) {
//            console.log(data);
//            var arr = $.parseJSON(data);
//            var canvas=document.getElementById('myCanvas');
//            var ctx=canvas.getContext('2d');
//            ctx.fillStyle='#996B7A';
//            ctx.fillRect(0,0,600,600);
//            ctx.fillStyle='#FF0000';
//            for (var dot in arr) {
//                console.log(arr[dot].x, arr[dot].y);
//                var x = parseInt(arr[dot].x) * 5;
//                var y = parseInt(arr[dot].y) * 5;
//                ctx.fillRect(x, y, 5, 5);
//            }
//
//            var historyUrl = url[1] + '/' + url[2] + '/' +url[3] + '/' + url[4];
//            console.log(historyUrl)
//            window.history.pushState('object or string', 'Title', historyUrl);
//            var newGenerationId = parseInt(url[6]) + 1;
//            var newNextGenerationUrl = url[0] + '//' + url[1] + url[2] + '/' +url[3] + '/' + url[4] + '/' + url[5] + '/' + newGenerationId;
//            var previousGenerationId = parseInt(url[6]) - 1;
//            var previousGenerationUrl = url[0] + '//' + url[1] + url[2] + '/' +url[3] + '/'
//            + url[4] + '/' + url[5] + '/' + previousGenerationId;
//            $('#generationId').text('Generation id is ' + url[6]);
//            $('#nextGeneration').attr('href', newNextGenerationUrl);
//            $('#previousGeneration').attr('href', previousGenerationUrl);
//            $(document).attr('title', 'Game generation '+url[6]);
//        });
//    }
//
//    $('#nextGeneration').click(function(event){
//        event.preventDefault();
//        var nextGenerationUrl = $('#nextGeneration').attr('href');
//        loadGeneration(nextGenerationUrl);
//    });
//
//    $('#previousGeneration').click(function(event){
//        event.preventDefault();
//        var previousGenerationUrl = $('#previousGeneration').attr('href');
//        loadGeneration(previousGenerationUrl);
//    });
//});