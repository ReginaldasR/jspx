$(document).ready(function() {
    $(".delete").css('display', 'block');
    $(".deleteForm").css('display', 'none');
});
$(document).on('click', 'a.delete', function(event){
    event.preventDefault();
    var url = $(event.currentTarget).attr("href");
    $.ajax({
        type: 'DELETE',
        url: url,
        beforeSend: function (request){
            request.setRequestHeader('accept', 'application/json');
        },
        complete : function ( data, textStatus, jqXHR ) {
        console.log(textStatus);
        console.log(data);
            $(".delete").css('display', 'block');
            $(".deleteForm").css('display', 'none');
//                $(event.currentTarget).closest("tr").remove();
            $('#postsTable').load('/posts/table');
      }
    })
});
