package lt.inventi.controllers;

import lt.inventi.config.ApplicationModule;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.sql.DataSource;

@Controller
public class SayNumberController {

    @RequestMapping(value = "/number/{number}")
    public ModelAndView number(@PathVariable(value = "number") Integer number){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationModule.class);
        DataSource db = context.getBean(DataSource.class);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        String sqlStatement = "insert into numbers(number) values (?)";
        jdbcTemplate.update(sqlStatement, number);
        return new ModelAndView("saynumber", "number", number);
    }

}
