package lt.inventi.controllers;

import lt.inventi.game.domain.GameRepository;
import lt.inventi.game.domain.Generation;
import lt.inventi.game.ui.HtmlTableDotFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class GameOfLifeController {


    private GameRepository gameRepository;

    @Autowired
    public GameOfLifeController(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @RequestMapping("/games")
    public String game() {
        return "games";
    }

    @RequestMapping("/game/{gameId}")
    public ModelAndView showGame(@PathVariable Integer gameId) {
        Generation gameConfiguration = gameRepository.findGame(gameId);
        StringBuilder generation = new StringBuilder();
        Map<String, CharSequence> map = new HashMap<String, CharSequence>();
        gameConfiguration.displayTo(new HtmlTableDotFormatter(), generation);
        String nextGen = "/game/" + gameId + "/generation/2";
        map.put("generation", generation);
        map.put("nextGen", nextGen);
        return new ModelAndView("game", "genInfo", map);
    }

    @RequestMapping("/game/{gameId}/generation/{genId}")
    public ModelAndView showGameGen(@PathVariable Integer gameId, @PathVariable Integer genId) {
        Generation gameConfiguration = gameRepository.findGame(gameId);
        for (int i = 1; i <= genId; i++) {
            gameConfiguration = gameConfiguration.next();
        }
        StringBuilder generation = new StringBuilder();
        Map<String, CharSequence> map = new HashMap<String, CharSequence>();
        gameConfiguration.displayTo(new HtmlTableDotFormatter(), generation);
        String nextGen = "/game/" + gameId + "/generation/" + (genId + 1);
        String previousGen = "/game/" + gameId + "/generation/" + (genId - 1);
        map.put("generation", generation);
        map.put("nextGen", nextGen);
        if (genId == 1) {
            previousGen = "";
        }
        map.put("previousGen", previousGen);
        return new ModelAndView("gameGen", "genInfo", map);
    }

}
