package lt.inventi.controllers;

import lt.inventi.dao.PostDao;
import lt.inventi.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller()
public class PostsController {

    private PostDao postDao;

    @Autowired
    public PostsController(PostDao postDao) {
        this.postDao = postDao;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ModelAndView home() {
        List<Post> posts = postDao.getPosts();
        return new ModelAndView("index", "posts", posts);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public String createForm() {
        return "/forms/create";
    }

    @RequestMapping(value = "/posts/create", method = RequestMethod.POST)
    public String create(@RequestParam String title, @RequestParam String text) {
        postDao.create(title, text);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/{postId}", params = {"_method=delete"})
    public String deletePost(@PathVariable Integer postId) {
        postDao.delete(postId);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)//params = {"_method=put"})
    public ModelAndView updateForm(@PathVariable Integer postId) {
        Post post = postDao.getPost(postId);
        return new ModelAndView("forms/updateForm", "post", post);
    }

    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.POST)
    public String update(@PathVariable Integer postId,
                         @RequestParam("title") String title, @RequestParam("text") String text) {
        postDao.update(postId, title, text);
        return "redirect:/posts";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/posts/{postId}", headers = "Accept=application/json")
    public void deleteAjax(@PathVariable Integer postId) {
        postDao.delete(postId);
    }

    @RequestMapping(value = "/posts/table")
    public ModelAndView postsViaAjax(Model model) {
        List<Post> posts = postDao.getPosts();
        model.addAttribute("posts", posts);
        return new ModelAndView("posts");
    }
}
