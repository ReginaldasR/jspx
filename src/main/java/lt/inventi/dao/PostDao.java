package lt.inventi.dao;

import lt.inventi.mapper.PostMapper;
import lt.inventi.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class PostDao{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public PostDao(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public void create(String title, String text) {
        String sqlStatement = "INSERT INTO posts (title, text) VALUES (?, ?)";
        jdbcTemplate.update(sqlStatement, title, text);
    }

    public Post getPost(Integer id) {
        String sqlStatement = "SELECT * FROM posts WHERE id = ?";
        return jdbcTemplate.queryForObject(sqlStatement, new Object[]{id}, new PostMapper());
    }

    public List<Post> getPosts() {
        String sqlStatement = "SELECT * FROM posts";
        return jdbcTemplate.query(sqlStatement, new PostMapper());
    }

    public void delete(Integer id) {
        String sqlStatement = "DELETE FROM posts WHERE id = ?";
        jdbcTemplate.update(sqlStatement, id);
    }

    public void update(Integer id, String title, String text) {
        String sqlStatement = "UPDATE posts set title = ?, text = ? WHERE id = ?";
        jdbcTemplate.update(sqlStatement, title, text, id);
    }
}
