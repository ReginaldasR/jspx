package lt.inventi.model;


import flexjson.JSONSerializer;

import java.util.Collection;

public class Post {

    private Integer id;
    private String title;
    private String text;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static String toJsonArray(Collection<Post> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
}
