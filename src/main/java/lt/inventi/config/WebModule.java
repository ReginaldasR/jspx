package lt.inventi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages={"lt.inventi"})
public class WebModule extends WebMvcConfigurerAdapter
{
	@Override
	public void addViewControllers(ViewControllerRegistry aRegistry)
	{
		aRegistry.addViewController("/").setViewName("index");
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry aRegistry)
	{
		aRegistry.addResourceHandler("/s/*").addResourceLocations("classpath:/META-INF/webapp/WEB-INF/static/js/*");
		aRegistry.addResourceHandler("/c/*").addResourceLocations("classpath:/META-INF/webapp/WEB-INF/static/css/*");
		aRegistry.addResourceHandler("/i/*").addResourceLocations("classpath:/META-INF/webapp/WEB-INF/static/images/*");
		aRegistry.addResourceHandler("/WEB-INF/views/*").addResourceLocations("classpath:/META-INF/webapp/WEB-INF/views/*");
		aRegistry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/META-INF/webapp/WEB-INF/static/images/favicon.ico");
	}
	
	@Bean
	public ViewResolver viewResolver() 
	{
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jspx");
		return viewResolver;
	}

    @Bean
    public HiddenHttpMethodFilter hidden () {

        return new HiddenHttpMethodFilter();
    }
}
