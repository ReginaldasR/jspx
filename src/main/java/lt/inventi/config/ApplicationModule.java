package lt.inventi.config;

import lt.inventi.dao.PostDao;
import lt.inventi.game.domain.Cell;
import lt.inventi.game.domain.GameRepository;
import lt.inventi.game.repository.GameRepositoryInMemory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import javax.sql.DataSource;

import static lt.inventi.game.domain.Generation.initial;

@Configuration
public class ApplicationModule {

//    @Bean
//    public DataSource dataSource() {
//        DriverManagerDataSource db = new DriverManagerDataSource();
//        db.setDriverClassName("org.sqlite.JDBC");
//        db.setUrl("jdbc:sqlite:/home/reginaldas/workspace/jetty-embedded-spring-mvc-noxml/src/main/resources/test");
//        return db;
//    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setDriverClassName("org.h2.Driver");
        db.setUrl("jdbc:h2:file://home/reginaldas/workspace/jetty-embedded-spring-mvc-noxml/src/main/resources/test2");
        return db;
    }

    @Bean
    public PostDao postDao() {
        return new PostDao(dataSource());
    }

    @Bean
    public GameRepository gameRepository () {
        GameRepository gameRepository = new GameRepositoryInMemory();
        gameRepository.insertStartingCells(1, initial(Cell.at(2, 2), Cell.at(3, 2), Cell.at(4, 2),
                Cell.at(4, 1), Cell.at(3, 0)));
        gameRepository.insertStartingCells(2, initial(Cell.at(2, 2), Cell.at(3, 2), Cell.at(4, 2)));
        return gameRepository;
    }


    @Bean
    public HiddenHttpMethodFilter hidden () {

        return new HiddenHttpMethodFilter();
    }

}
