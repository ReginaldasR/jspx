package lt.inventi.game.ui
/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/29/13
 * Time: 9:21 AM
 */
public class HtmlTableDotFormatter implements DotFormatter {

    @Override
    String format(Set<Dot> dotsToShow, int xMin, int xMax, int yMin, int yMax) {
        StringBuilder htmlOutput = new StringBuilder()
        for (int i = xMin - 1; i <= xMax + 1; i++) {
            htmlOutput.append("<tr>")
            for (int j = yMin - 1; j <= yMax + 1; j++) {
                boolean isCellAlive = false;
                for (Dot dot : dotsToShow)
                    if (dot.x == i && dot.y == j) {
                        isCellAlive = true;
                    }
                if (isCellAlive) {
                    htmlOutput.append("<td class='notEmpty'> </td>")
                } else {
                    htmlOutput.append("<td class='empty'> </td>")
                }
            }
            htmlOutput.append("</tr>\n")
        }
        return htmlOutput.toString()
    }
}