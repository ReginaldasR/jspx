package lt.inventi.game.ui
/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/23/13
 * Time: 2:12 PM
 */

public class Dot  {

    public final int x
    public final int y

    public Dot(int x, int y) {
        this.x = x
        this.y = y
    }

    boolean equals(Object object) {
        if (this.is(object)) {
            return true
        }
        if (getClass() != object.class) {
            return false
        }
        Dot dot = (Dot) object
        if (x != dot.x || y != dot.y) {
            return false
        }
        return true
    }

    int hashCode() {
        31 * x + y
    }


    @Override
    public java.lang.String toString() {
        return "Dot{" +
            "x=" + x +
            ", y=" + y +
            '}';
    }
}