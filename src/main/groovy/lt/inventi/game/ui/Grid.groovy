package lt.inventi.game.ui

import groovy.transform.EqualsAndHashCode

import static java.lang.Integer.MAX_VALUE
import static java.lang.Integer.MIN_VALUE
import static java.lang.Math.max
import static java.lang.Math.min
/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/23/13
 * Time: 2:11 PM
 */
@EqualsAndHashCode
public class Grid {

    protected Set<Dot> dots = new HashSet<>();

    Grid (Dot... dots) {
        this.dots = dots as Set
    }

    public void createNewDot(int x, int y) {
        dots.add(new Dot(x, y));
    }

    String showIn(DotFormatter out) {
        int xMax = MIN_VALUE
        int xMin = MAX_VALUE
        int yMax = MIN_VALUE
        int yMin = MAX_VALUE

        for (Dot dot : dots) {
            xMax = max(xMax, dot.x)
            xMin = min(xMin, dot.x)
            yMax = max(yMax, dot.y)
            yMin = min(yMin, dot.y)
        }
        return out.format(dots, xMin, xMax, yMin, yMax);
    }

    @Override
    public String toString() {
        return "Grid{" +
            "dots=" + dots +
            '}';
    }

    int[] count() {
        int xMax = MIN_VALUE
        int xMin = MAX_VALUE
        int yMax = MIN_VALUE
        int yMin = MAX_VALUE

        for (Dot dot : dots) {
            xMax = max(xMax, dot.x)
            xMin = min(xMin, dot.x)
            yMax = max(yMax, dot.y)
            yMin = min(yMin, dot.y)
        }
        [xMin, xMax, yMin, yMax]
    }

    List<Integer> width() {

        int xMax = MIN_VALUE
        int xMin = MAX_VALUE
        List<Integer> width = new ArrayList<Integer>()
        for (Dot dot : dots) {
            xMax = max(xMax, dot.x)
            xMin = min(xMin, dot.x)
        }
        for (int i = xMin-1; i <= xMax+1; i++){
            width.add(i)
        }
        width
    }

    List<Integer> height() {
        int yMax = MIN_VALUE
        int yMin = MAX_VALUE
        List<Integer> height = new ArrayList<Integer>()
        for (Dot dot : dots) {
            yMax = max(yMax, dot.y)
            yMin = min(yMin, dot.y)
        }
        for (int i = yMin-1; i <= yMax+1; i++){
            height.add(i)
        }
        height
    }

    boolean containsDot(int x, int y) {
        dots.contains(new Dot(x ,y))
    }
}