package lt.inventi.game.ui
/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/23/13
 * Time: 2:11 PM
 */
public interface DotFormatter {

    String format(Set<Dot> dotsToShow, int xMin, int xMax, int yMin, int yMax);

}