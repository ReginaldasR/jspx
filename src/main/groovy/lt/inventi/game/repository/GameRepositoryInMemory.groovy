package lt.inventi.game.repository

import lt.inventi.game.domain.GameRepository
import lt.inventi.game.domain.Generation

/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/28/13
 * Time: 5:37 PM
 */
public class GameRepositoryInMemory implements GameRepository {

    protected def startingCellsMap = new HashMap()

    @Override
    void insertStartingCells (int gameId, Generation startingCells) {
        startingCellsMap[gameId] = startingCells
    }

    @Override
    Generation findGame(int gameId) {
        startingCellsMap[gameId] as Generation
    }
}