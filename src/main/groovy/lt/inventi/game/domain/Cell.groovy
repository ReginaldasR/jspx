package lt.inventi.game.domain

import com.google.common.collect.ImmutableSet
import lt.inventi.game.ui.Grid

/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/23/13
 * Time: 2:09 PM
 */
public class Cell {

    protected final int x;
    protected final int y;

    private Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Cell at(int x, int y) {
        new Cell(x, y);
    }

    public Set<Cell> getNeighbours() {
        ImmutableSet.of(Cell.at(x - 1, y - 1), Cell.at(x - 1, y), Cell.at(x - 1, y + 1)
            , Cell.at(x, y - 1), Cell.at(x, y + 1)
            , Cell.at(x + 1, y - 1), Cell.at(x + 1, y), Cell.at(x + 1, y + 1));
    }

    public void putTo(Grid grid) {
        grid.createNewDot(x, y);
    }

    @Override
    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Cell cell = (Cell) o

        if (x != cell.x) return false
        if (y != cell.y) return false

        true
    }

    @Override
    int hashCode() {
        int result
        result = x
        result = 31 * result + y
        result
    }

    @Override
    public String toString() {
        x + " " + y;
    }

}