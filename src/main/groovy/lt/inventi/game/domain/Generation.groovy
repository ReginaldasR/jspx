package lt.inventi.game.domain

import lt.inventi.game.ui.DotFormatter
import lt.inventi.game.ui.Grid

import static com.google.common.collect.Sets.union
import static java.util.Collections.addAll
/**
 * User: Reginaldas Raila
 * Email: ReginaldasR@gmail.com
 * Date: 5/23/13
 * Time: 2:10 PM
 */
public class Generation {

    protected final Set<Cell> aliveCells = new HashSet<Cell>();

    static Generation initial(Cell... cells) {
        new Generation(cells);
    }

    protected Generation(Cell... cells) {
        addAll(aliveCells, cells);
    }

    protected Generation(Collection<Cell> aliveCellsSet) {
        aliveCells.addAll(aliveCellsSet);
    }

    Generation next() {
        Set<Cell> survivingCells = aliveCells.findAll{
            (2..3).contains(it.getNeighbours().intersect(aliveCells).size())
        }
        Set<Cell> newBornCells = allCellNeighbours().findAll{
            it.getNeighbours().intersect(aliveCells).size() == 3
        }
        new Generation(union(survivingCells, newBornCells))
    }

    Set<Cell> allCellNeighbours () {
        aliveCells.collectMany {it.getNeighbours()} as Set<Cell>
    }

    void displayTo(DotFormatter formatter, Appendable output) {
        Grid grid = new Grid();
        for (Cell cell : aliveCells) {
            cell.putTo(grid);
        }
        output.append grid.showIn(formatter)
    }

    @Override
    String toString() {
        String string = "";
        for (Cell cell : aliveCells) {
            string += " " + cell.toString();
        }
        string;
    }

    @Override
    boolean equals(Object obj) {
        if (this.is(obj)) {
            return true
        }
        if (obj == null || this.getClass() != obj.class) {
            return false
        }
        Generation generation = (Generation) obj
        aliveCells == generation.aliveCells
    }

    @Override
    int hashCode() {
        aliveCells.hashCode()
    }

    Grid toGrid() {
        Grid grid = new Grid();
        for (Cell cell : aliveCells) {
            cell.putTo(grid);
        }
        return grid
    }
}