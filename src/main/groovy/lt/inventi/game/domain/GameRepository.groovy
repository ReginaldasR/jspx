package lt.inventi.game.domain

public interface GameRepository {

    void insertStartingCells (int gameId, Generation startingCells)

    Generation findGame (int gameId)

}